import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { DmnEditorComponent } from './dmn-editor/dmn-editor.component';

@NgModule({
  declarations: [
    AppComponent,
    DmnEditorComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
